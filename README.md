
[![build status](https://gitlab.com/bitfireAT/ical4android/badges/master/build.svg)](https://gitlab.com/bitfireAT/ical4android/builds)


# ical4android

ical4android is an Android library that brings together iCalendar and Android.
It's a framework for

* parsing and generating iCalendar resources (using [ical4j](https://github.com/ical4j/ical4j))
  from/into data classes that are compatible with the Android Calendar Provider and
  third-party task providers,
* accessing the Android Calendar Provider (and third-party task providers) over a unified API.

It has been primarily developed for:

* [DAVdroid](https://www.davdroid.com)
* [ICSdroid](https://icsdroid.bitfire.at)

Generated KDoc: https://bitfireAT.gitlab.io/ical4android/dokka/ical4android/


## Contact

```
bitfire web engineering – Stockmann, Hirner GesnbR
Florastraße 27
2540 Bad Vöslau, AUSTRIA
```

Email: [play@bitfire.at](mailto:play@bitfire.at) (do not use this)

For questions, suggestions etc. please use the DAVdroid forum:
https://www.davdroid.com/forums/


## License 

Copyright (C) bitfire web engineering (Ricki Hirner, Bernhard Stockmann).

This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome
to redistribute it under the conditions of the [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html).

